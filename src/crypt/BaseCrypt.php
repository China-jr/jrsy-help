<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace jrsy\help\crypt;


use jrsy\help\string\Str;

/**
 * Crypt 加密实现类
 */

class BaseCrypt {

    public $config = [];

    public $key = '';

    public $strLen = 0;

    public $data = '';

    public $rsa = '';

    public $string = '';


    public function __construct($config){
        $this->config = $config;
        $this->key = isset($config['key'])?$config['key']:'';
        $this->rsa = new Rsa($this->config);
        $this->string = new Str();
    }


    public function errCode($code){
        return ['code'=>$code,'message'=>RsaCodeEnum::$OBJECT[$code],'data'=>[]];
    }


}
