<?php


namespace jrsy\help\pay\wxpay;


class Notify extends BaseWxpay
{


    public function __construct() {

        $xmlData = $this->getPost();

        $arr = $this->XmlToArr($xmlData);

        if($this->checkSign($arr)){

            if($arr['return_code'] == 'SUCCESS' && $arr['result_code'] == 'SUCCESS'){
                //生产环境需要根据订单号来查询价格
                if($arr['total_fee'] == 2){
                    $this->logs('stat.txt', '交易成功!'); //更改订单状态
                    $returnParams = [
                        'return_code' => 'SUCCESS',
                        'return_msg'  => 'OK'
                    ];
                    echo $this->ArrToXml($returnParams);
                }else{
                    $this->logs('stat.txt', '金额有误!');
                }
            }else{
                $this->logs('stat.txt', '业务结果不正确!');
            }
        }else{
            $this->logs('stat.txt', '签名失败!');
        }
    }

}
