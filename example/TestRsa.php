<?php

use jrsy\help\crypt\Rsa;

include '../vendor/autoload.php';

$config['public_key'] = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/dhE3ask29Yf+jzpRq3j2K1f5
shymX5/T9EGUQx7u5XdEQhzVULJBQRCtTnot5lbVdesS+JSA5NlSflBg/LBbHEn7
fwH2RFN882zUVZ6WVLQcMju2Kq7O31fwvd7uQMrxan81fp91O3UoA8iudI0rzTVq
9lMPfnkVszdZoS2mAwIDAQAB
-----END PUBLIC KEY-----';

$config['private_key'] ='-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDIHaoBfzbLw+szQ4hcaA+bqkb+20zTXzwdLQdZm2RlWcb2Ftra
/tlK+JMQog7TeQLXe4rnT0ZqUe234DCd7nE4h7Lg6ADMe4+IcW9zo2ZzlG/AIV+z
wOnnwBxieuUnPWAAMfS52drWXBD70lM48lS0f1Mu02E39aJF40YLVor6ZQIDAQAB
AoGAAXOQS258G+znLySq6oX+sacB2NCyX/Nti4X2Fa+g13NX6WpaWq2ahIavr7aH
26ohsSUTlCs+XCsNSat17s9PQZHA5qvM61nsq/JaICTUTAwQSQAz4/C87hWHa9FL
EONrd6O161++8M+jM9NrRWRrZQxl/4igAkRJAYNSUoDwCYECQQDteQrNm5eDi9s8
58BAo7C8dleIP3TbuCdCyKx/3NUNKSSjPoWp1DS2INr8vSXjf+lJm6kixbL5MUuQ
KiBs47NVAkEA17qBWXLyzxIbmWIXw3c/mPZSbv8i6Qi8puI+wGQjnBh8fm7jOTEO
2I5ly6kGqHAwNpbitM2qKyZnqEqwEZNK0QJAKz4lZ3dzybKIA7k32w1zu33ZRqg3
gWMxUwnzz3zJFxJs54UVFe3cg2XMOsW+xoSeWJcN35UcmCAP4HqrWOjF2QJBAKvH
FeJ9FnzuZU2gf12k4d5SHdfGjyzSSS31r5QQ6Q4NrWwbrhFHUBP+Z25hPCz5kFqX
kRP5REwxu46VUlfcWlECQQDNWg+GnWoxg3SqUqDPJBmcurK3FIpvtCJkikvW02Cc
H7WDrFbA3bquM9+eiTyYRPHxafmg4Fk8HTj9utnNB+bY
-----END RSA PRIVATE KEY-----';

$token = $_SERVER['HTTP_TOKEN'];


$rsa = new Rsa($config);

$rtn = $rsa->privateDecrypt($token);

$rtns = $rsa->publicEncrypt($rtn['data']);

echo $rtns['data'];

//$data = array_keys($_REQUEST);
//$rtn1 = $rsa->publicDecrypt($data);
//var_dump($rtn1);

